server '45.32.109.54', user: 'deploy', roles: %w{app db web}

append :linked_files, "db/production.sqlite3"

namespace :deploy do
  desc 'Source bash'
  task :source do
    on roles(:app), in: :sequence, wait: 5 do
      execute "source ~/.bashrc"
    end
  end
  after :log_revision, :source
end
