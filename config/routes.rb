Rails.application.routes.draw do
  mount Lines::Engine => "/blog"
  get '/blog/article/:id' => "lines/articles#show", as: 'article'
  get '/about' => 'home#about'
  get '/services' => 'home#services'
  get '/contact' => 'home#contact'
  get '/careers' => 'home#careers'
  post '/enquiry' => 'home#enquiry'
  root to: 'home#index'
end
