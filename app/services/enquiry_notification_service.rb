class EnquiryNotificationService
  attr_reader :params, :slackbot
  EMAIL_REGEXP = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  def initialize(params)
    @params = params
    @slackbot = Slack::Web::Client.new(token: ENV['SLACK_API_TOKEN'])
  end

  def valid?
    enquiry_valid? && email_format_valid?
  end

  def notify
    slackbot.chat_postMessage(channel: 'enquiries', text: build_message)
  end

  private

  def build_message
    "
      Hi <!here>, We've just received a new enquiry from *#{params[:email]}*
      *Full name:*
      > #{params[:name]}
      *Message:*
      > #{params[:message]}
      *Time:* 
      > #{Time.now.strftime("%d-%m-%Y %H:%M:%S")}
    "
  end

  def enquiry_valid?
    params[:name].present? && params[:message].present?
  end

  def email_format_valid?
    (params[:email] =~ EMAIL_REGEXP).present?
  end
end
