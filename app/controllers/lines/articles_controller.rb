# Lists all Articles and shows specific ones.
# Responds either to HTML and ATOM requests.
require 'acts-as-taggable-on'
require_dependency "lines/application_controller"

module Lines

  class ArticlesController < ApplicationController

    layout 'lines/blogs'

    KEYWORDS = CONFIG[:keywords]
    SITE_TITLE = CONFIG[:title]

    # Lists all published articles.
    # Responds to html and atom
    def index
      @tags = ActsAsTaggableOn::Tag.distinct.pluck(:name)
      @tag = params[:tag] || 'all'
      @current_page = params[:page].to_i
      if params[:tag]
        @articles = Lines::Article.published.tagged_with(params[:tag]).page(params[:page].to_i)
      else
        @articles = Lines::Article.published.page(params[:page].to_i)
      end
      respond_to do |format|
        format.html do
          set_meta_tags title: SITE_TITLE,
                        description: CONFIG[:meta_description],
                        keywords: KEYWORDS,
                        open_graph: { title: SITE_TITLE,
                                        type: 'website',
                                        url: articles_url,
                                        site_name: SITE_TITLE,
                                        image: CONFIG[:og_logo]
                                      }
        end
        format.js
        format.atom do
          @articles = Lines::Article.published
        end
      end
    end

    def show
      @article = Lines::Article.published.find(params[:id])
      meta_tags = { title: @article.title,
        type: 'article',
        url: url_for(@article),
        site_name: SITE_TITLE,
      }
      meta_tags[:image] = CONFIG[:host] + @article.image_url if @article.image_url.present?
      set_meta_tags title: @article.title,
                    keywords: KEYWORDS + @article.tag_list.to_s,
                    open_graph: meta_tags

      @related_articles = Lines::Article.published.where('id != ?', @article.id).order('').limit(2)
    end
  end
end
