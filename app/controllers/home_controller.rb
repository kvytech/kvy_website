class HomeController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    @nav_class = "absolute transparent"
  end

  def about
    @nav_class = "absolute transparent"
  end

  def services
    @nav_class = "absolute transparent"
  end

  def contact
  end

  def blog
  end

  def appointment
  end

  def careers
  end

  def enquiry
    service = EnquiryNotificationService.new(params)
    
    if service.valid?
      service.notify
      flash[:success] = "Thank you! We’ll get back to you as soon as possible at #{params[:email]}"
      redirect_to root_path
    else
      flash[:warning] = "There was an error sending your message. Please try again."
      redirect_to contact_path
    end
  end
end
