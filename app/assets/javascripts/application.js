// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.min
//= require bootstrap
//= require googletagmanager
//= require lozad.min
//= require lazy
$(document).ready(function(){
  var btnContainer = document.getElementById("header");
  var btns = btnContainer.getElementsByClassName("nav-item");
  for (var i = 0; i < btns.length; i++) {
    if (i === 0 && window.location.pathname === "/services") {
      btns[i].className += " active"
    } else if (i === 1 && window.location.pathname === "/about" ) {
      btns[i].className += " active"
    } else if (i === 2 && window.location.pathname === "/blog" ) {
      btns[i].className += " active"
    } else if (i === 4 && window.location.pathname === "/careers" ) {
      btns[i].className += " active"
    } else if (i === 5 && window.location.pathname === "/contact" ) {
      btns[i].className += " active"
    }
  }
  $('.crcontent__apply').click(function(){
    var win = window.open("https://kvytechnology.freshteam.com/jobs", '_blank');
    win.focus();
  })
});
