let clientCarousel = document.querySelectorAll(".client-carousel-list > li");
let clientCarouselLength = clientCarousel.length;
let clientCarouselItem = document.querySelectorAll(".client-carousel-item");
let clientCarouselItemLength = clientCarouselItem.length;
for (let i = 0; i < clientCarouselLength; i++) {
  clientCarousel[i].onclick = function() {
    for (let k = 0; k < clientCarouselLength; k++) {
      clientCarousel[k].classList.remove("active");
      clientCarouselItem[k].removeAttribute("style");
    }
    clientCarouselItem[i].style.display = "flex";
    this.classList.add("active");
  };
}

(initial = () => {
  clientCarousel[2].classList.add("active");
  clientCarouselItem[2].style.display = "flex";
})();
